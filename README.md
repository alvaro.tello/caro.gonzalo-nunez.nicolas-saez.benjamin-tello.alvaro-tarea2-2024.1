DISEÑO Y PROGRAMACIÓN ORIENTADA A OBJETOS (ELO - 329)
DEPARTAMENTO DE ELECTRONICA
UNIVERSIDAD TÉCNICA FEDERICO SANTA MARÍA

# TAREA Nº2: Simulación de una mascota virtual en JavaFX

Estudiantes:        
- Gonzalo Caro    ROL: 202030553 - 6
- Nicolas Núñez   ROL: 202130502 - 5
- Benjamín Saez   ROL: 202173616 - 6
- Álvaro Tello    ROL: 201930552 - 2

# I. Concepto de Tarea y Funcionamiento

La tarea consiste en la simulación del comportamiento de una mascota virtual. La cual posee ciertas características y comportamientos los cuales son modelados mediante el programa. Esta posee estados de animo que pueden variar y alterar su comportamiento según sus atributos. Además de poder interactuar con items los cuales pueden modificar dichos atributos con el mismo fin. Estos items se manejan en base a un sistema de inventario el cual de la misma forma se encuentra implementado en el programa. 
El desarrollo del código y el funcionamiento del programa se dividió en 4 etapas. Cada una de ellas formando parte de un desarrollo incremental. Esto quiere decir que todas las funcionalidades de la etapa 1 funcionan en la etapa 2. 

En términos de contenidos para cada etapa tendremos:
i. Etapa 1: Creacion de la mascota con sus atributos y estado.
ii. Etapa 2: Se diseñan y implementan los items.
iii. Etapa 3: Se crea y implementa el inventario. 
iv. Etapa 4: Se inicializa el inventario y se manipula mediante archivo csv.

Es importante destacar los siguientes aspectos:
El programa se iniciará con el inventario según la información indicada por el archivo csv.
Para hacer avanzar el tiempo en el programa se debe ingresar mediante el input la letra "c".
Y al ingresar el número del objeto, este será consumido y restado del inventario. 

# II. Ejecución del Programa

El procedimiento para ejecutar el programa es el siguiente.

Para compilar se debe ejecutar el comando: $ make
Luego se ejecuta con: $ make run
Y si desea eliminar los archivos .class utilice: $ make clean

En su defecto, se puede compilar con el comando: javac -g -d bin Main.java
Y para ejecutarlo utilizar el comando: java -cp bin Main config.csv

Recordar que para el correcto funcionamiento del programa todos los archivos deben estar en el mismo directorio. 