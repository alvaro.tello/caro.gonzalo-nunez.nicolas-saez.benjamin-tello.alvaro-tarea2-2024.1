package com.example.mascotavirtual;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
//Representación gráfica de item consumible
public class Consumible extends HBox {
    Consumible(Item item, Inventario inventario, Mascota mascota){
        super();
        Label label_cantidad = new Label();
        label_cantidad.textProperty().bind(item.get_cantidad().asString());
        Button boton_nombre = new Button(item.get_nombre());
        boton_nombre.setOnAction(e -> mascota.usar_item(inventario.sacar_item(item.get_id())));
        label_cantidad.setMinWidth(30);
        label_cantidad.setMinHeight(boton_nombre.getMinHeight());
        label_cantidad.setAlignment(Pos.CENTER);
        this.getChildren().addAll(label_cantidad, boton_nombre);
    }
}