package com.example.mascotavirtual;

import javafx.application.*;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.*;

import java.io.FileInputStream;
import java.io.IOException;

public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        Controller controller = new Controller();
        // Menu bar ----------
        MenuBar menuBar = new MenuBar();
        Menu menuInicio = new Menu("Inicio");
        Menu menuAcciones = new Menu("Acciones");
        Menu menuHelp = new Menu("Help");

        MenuItem iniciar = new MenuItem("Iniciar");
        MenuItem reiniciar = new MenuItem("Reiniciar");
        MenuItem salir = new MenuItem("Salir");
        menuInicio.getItems().addAll(iniciar, reiniciar, salir);

        MenuItem apagar = new MenuItem("Apagar luz");
        MenuItem prender = new MenuItem("Prender luz");
        menuAcciones.getItems().addAll(apagar, prender);

        MenuItem acerca = new MenuItem("Acerca de");
        menuHelp.getItems().addAll(acerca);

        menuBar.getMenus().addAll(menuInicio, menuAcciones, menuHelp);
        // ----------------

        // Info nombre y edad ----------------
        Label nombre_label = new Label("Nombre:");
        Label nombre = new Label();
        Label edad_label = new Label("Edad:");
        Label edad = new Label();

        nombre_label.setFont(new Font(15));
        nombre.setFont(new Font(15));
        edad_label.setFont(new Font(15));
        edad.setFont(new Font(15));

        GridPane info_nombre_edad = new GridPane();

        info_nombre_edad.setVgap(5);
        info_nombre_edad.setHgap(10);

        info_nombre_edad.add(nombre_label, 0,0);
        info_nombre_edad.add(nombre, 1,0);
        info_nombre_edad.add(edad_label, 0,1);
        info_nombre_edad.add(edad, 1,1);
        //--------------------

        // Barras ----------------------------
        Label salud_titulo = new Label("Salud");
        Label energia_titulo = new Label("Energía");
        Label felicidad_titulo = new Label("Felicidad");

        ProgressBar salud_barra = new ProgressBar();
        ProgressBar energia_barra = new ProgressBar(0.5);
        ProgressBar felicidad_barra = new ProgressBar(0.6);

        VBox salud = new VBox(salud_titulo, salud_barra);
        VBox energia = new VBox(energia_titulo, energia_barra);
        VBox felicidad = new VBox(felicidad_titulo, felicidad_barra);

        VBox barras = new VBox(salud,energia,felicidad);
        barras.setSpacing(5);
        // ----------------------

        // Estado --------------
        Label estado_titulo = new Label("Estado:");
        estado_titulo.setFont(new Font(25));
        Label estado = new Label();
        estado.setFont(new Font(20));
        VBox estado_box = new VBox(estado_titulo, estado);
        //-------------------------

        // Sección izquierda
        VBox izquierda = new VBox(info_nombre_edad, barras, estado_box);
        izquierda.setSpacing(12);


        // Vista del juego ------------------
        Image fondo_img_dia = new Image(new FileInputStream("images/background/background.png"));
        ImageView fondo_dia = new ImageView(fondo_img_dia);
        fondo_dia.setPreserveRatio(true);
        fondo_dia.setFitWidth(500);
        fondo_dia.setFitHeight(500);

        Image fondo_img_noche = new Image(new FileInputStream("images/background/sleep.png"));
        ImageView fondo_noche = new ImageView(fondo_img_noche);
        fondo_noche.setPreserveRatio(true);
        fondo_noche.setFitWidth(500);
        fondo_noche.setFitHeight(500);

        Image mascota_img = new Image(new FileInputStream("images/pet/mascota.png"));
        ImageView mascota_view = new ImageView(mascota_img);
        mascota_view.setPreserveRatio(true);
        mascota_view.setFitWidth(165);
        mascota_view.setFitHeight(165);
        mascota_view.setTranslateY(fondo_dia.getFitHeight()/4);

        StackPane vista_juego = new StackPane(fondo_noche, fondo_dia, mascota_view);
        // ----------------------------------

        // Inventario ----------------------
        GridPane grid_alimentos = new GridPane();
        GridPane grid_medicina = new GridPane();
        GridPane grid_juguetes = new GridPane();

        grid_alimentos.setGridLinesVisible(true);
        grid_medicina.setGridLinesVisible(true);

        Label titulo_alimentos = new Label("Alimentos");
        Label titulo_medicina = new Label("Medicina");
        Label titulo_juguetes = new Label("Juguetes");

        titulo_alimentos.setAlignment(Pos.CENTER);
        titulo_medicina.setAlignment(Pos.CENTER);
        titulo_juguetes.setAlignment(Pos.CENTER);

        VBox inventario_alimentos = new VBox(titulo_alimentos, grid_alimentos);
        VBox inventario_medicina = new VBox(titulo_medicina, grid_medicina);
        VBox inventario_juguetes = new VBox(titulo_juguetes, grid_juguetes);

        inventario_alimentos.setMinWidth(200);
        inventario_medicina.setMinWidth(200);
        inventario_medicina.setMinWidth(200);

        HBox container_inventarios = new HBox(inventario_alimentos,inventario_medicina,inventario_juguetes);
        ScrollPane inventarios = new ScrollPane(container_inventarios);
        // -----------------------------------------------

        // Parte derecha
        VBox derecha = new VBox(vista_juego, inventarios);

        // Layout general
        HBox separacion = new HBox(izquierda, derecha);
        BorderPane principal = new BorderPane();
        principal.setTop(menuBar);
        principal.setLeft(izquierda);
        principal.setCenter(derecha);

        //Binds a propiedades
        controller.bind_menu_inicio(iniciar, reiniciar, salir, stage);
        controller.bind_barras(salud_barra, energia_barra, felicidad_barra);
        controller.bind_menu_acciones(apagar, prender, fondo_dia, fondo_noche);
        controller.bind_info(nombre, edad, estado);
        controller.rellenar_grids(grid_alimentos, grid_medicina, grid_juguetes);

        //Scene
        Scene scene = new Scene(principal, 780, 650);
        stage.setTitle("Mascota virtual");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
