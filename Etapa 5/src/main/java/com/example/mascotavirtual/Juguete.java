package com.example.mascotavirtual;

public class Juguete extends Item{
    private String path_imagen;
    public Juguete(Integer id, String nombre, String path_imagen){
        super(id, nombre, -1);
        this.felicidad = 30;
        this.path_imagen = path_imagen;
    }

    String get_path_img(){return path_imagen;}
}