package com.example.mascotavirtual;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

// Clase encargada de la conexión entre Interfaz Gráfica y Mascota
public class Controller {

    private Mascota mascota;
    private Inventario inventario;
    private BooleanProperty pausado;
    private Timeline timeline;
    private Timeline timeline_mov;

    public Controller() throws FileNotFoundException {
        inventario = new Inventario();
        Scanner in = new Scanner(new File("config/config.csv"));
        mascota = new Mascota(in.nextLine());
        //Lee del archivo config.csv
        while(in.hasNextLine()){
            String linea = in.nextLine();
            String[] split_linea = linea.split(";");
            int id = Integer.parseInt(split_linea[0]);
            String tipoItem = split_linea[1];
            String nombreItem = split_linea[2];

            Item item = null;
            int cantidad = 0;
            switch(tipoItem){
                case "Juguete":
                    String path_imagen = split_linea[3];
                    item = new Juguete(id, nombreItem, path_imagen);
                    break;
                case "Alimento":
                    cantidad = Integer.parseInt(split_linea[3]);
                    item = new Comida(id, nombreItem, cantidad);
                    break;
                case "Medicina":
                    cantidad = Integer.parseInt(split_linea[3]);
                    item = new Medicina(id, nombreItem, cantidad);
                    break;
            }
            inventario.agregar_items(item);
        }

        pausado = new SimpleBooleanProperty(true);

        //Ticks de tiempo
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),
                e -> mascota.timeStep(),
                (KeyValue) null));

        //Animación de mascota
        timeline_mov = new Timeline();
        timeline_mov.setCycleCount(Timeline.INDEFINITE);
        timeline_mov.setAutoReverse(true);
    }

    //Agrega items del inventario a los GridPanes
    public void rellenar_grids(GridPane alimentos, GridPane medicina, GridPane juguete) throws FileNotFoundException {
        inventario.rellenar_grids(alimentos, medicina, juguete, mascota);
    }

    //Configura los botones del menú de inicio
    public void bind_menu_inicio(MenuItem iniciar, MenuItem reiniciar, MenuItem salir, Stage stage){
        iniciar.setOnAction(e -> {
            timeline.play();
            timeline_mov.play();
            pausado.set(false);
        });
        iniciar.visibleProperty().bind(pausado);

        reiniciar.setOnAction(e -> {
            mascota.reset();
            inventario.reset();
            timeline.stop();
            timeline_mov.pause();
            pausado.set(true);
        });
        reiniciar.visibleProperty().bind(pausado.not());
        salir.setOnAction(e -> stage.close());
    }

    //Configura los botones del menú de acciones, incluyendo la acción de cambiar a dia/noche las imágenes
    public void bind_menu_acciones(MenuItem apagar, MenuItem prender, ImageView fondo_dia, ImageView fondo_noche){
        apagar.setOnAction(e -> {
            mascota.dormir();
        });
        apagar.visibleProperty().bind(mascota.isDormida().not());

        prender.setOnAction(e -> {
            mascota.despertar();
        });
        prender.visibleProperty().bind(mascota.isDormida());

        fondo_dia.visibleProperty().bind(mascota.isDormida().not());
        fondo_noche.visibleProperty().bind(mascota.isDormida());
    }

    //Configura el botón de about
    public void bind_menu_ayuda(MenuItem ayuda, Stage acerca_de){
        ayuda.setOnAction(e -> acerca_de.show());
    }

    //Bind a las propiedades de la mascota con las barras
    public void bind_barras(ProgressBar salud, ProgressBar energia, ProgressBar felicidad){
        salud.progressProperty().bind(mascota.getSalud().divide(100));
        energia.progressProperty().bind(mascota.getEnergia().divide(100));
        felicidad.progressProperty().bind(mascota.getFelicidad().divide(100));
    }

    //Bind a propiedades de texto de la mascota con los labels
    public void bind_info(Label nombre, Label edad, Label estado){
        nombre.textProperty().bind(mascota.getNombre());
        edad.textProperty().bind(mascota.getEdad().asString());
        estado.textProperty().bind(mascota.getEstado());
    }

    //Configura la animación de la mascota
    public void animacion(ImageView imagen_mascota){
        imagen_mascota.setTranslateX(-150);
        KeyFrame movimiento = new KeyFrame(Duration.millis(2000),
                new KeyValue (imagen_mascota.translateXProperty(), 150));
        timeline_mov.getKeyFrames().add(movimiento);
        timeline_mov.rateProperty().bind(mascota.getVelocidad());
    }

}
