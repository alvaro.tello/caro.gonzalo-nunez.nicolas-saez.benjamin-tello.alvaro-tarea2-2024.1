package com.example.mascotavirtual;


import javafx.beans.property.*;
// Clase que define a la mascota
public class Mascota {

    // Atributos de tipo Property, para ser conectados con la interfaz
    private StringProperty nombre;
    private StringProperty estadoText;
    private DoubleProperty edad;
    private FloatProperty salud;
    private FloatProperty energia;
    private FloatProperty felicidad;
    private FloatProperty velocidad;
    private BooleanProperty dormida;

    //Atributo interno
    private Estado estado;

    // Máximos valores para los indicadores
    public final int SALUD_MAXIMA = 100;
    public final int ENERGIA_MAXIMA = 100;
    public final int FELICIDAD_MAXIMA = 100;

    public Mascota(String nombre) {
        this.nombre = new SimpleStringProperty(nombre);
        this.edad = new SimpleDoubleProperty(0.0);
        this.salud = new SimpleFloatProperty(SALUD_MAXIMA);
        this.energia = new SimpleFloatProperty(ENERGIA_MAXIMA);
        this.felicidad = new SimpleFloatProperty(FELICIDAD_MAXIMA);
        this.estadoText = new SimpleStringProperty();
        this.velocidad = new SimpleFloatProperty();
        this.dormida = new SimpleBooleanProperty(false);
        actualizar_estado();
    }

    //Resetea los valores de la mascota a los iniciales
    public void reset(){
        edad.set(0.0);
        salud.set(SALUD_MAXIMA);
        energia.set(ENERGIA_MAXIMA);
        felicidad.set(FELICIDAD_MAXIMA);
        dormida.set(false);
        actualizar_estado();
    }


    //Determina el estado en el que debería estar la mascota
    private Estado determinar_estado(){
        if (salud.getValue() <= 0 || energia.getValue() <= 0 || edad.getValue() >= 15)
            return Estado.Muerto;
        if (energia.getValue() <= 15)
            return Estado.Cansado;
        if (edad.getValue() > 5f && salud.getValue() <= 30 && energia.getValue() <= 30)
            return Estado.Enojado;
        if (edad.getValue() <= 5f && salud.getValue() <= 20 ||
            edad.getValue() > 5f && edad.getValue() <= 10f && salud.getValue() <= 50)
            return Estado.Hambriento;
        if (felicidad.getValue() <= 20)
            return Estado.Triste;
        if (felicidad.getValue() >= 60)
            return Estado.Feliz;
        return Estado.Neutro;
    }

    //Convierte el estado de la mascota a string
    private String estado_string(){
        return switch (estado) {
            case Muerto -> "Muerto";
            case Cansado -> "Cansado";
            case Enojado -> "Enojado";
            case Hambriento -> "Hambriento";
            case Triste -> "Triste";
            case Feliz -> "Feliz";
            default -> "Neutro";
        };
    }

    //Determina la velocidad de movimiento de la mascota
    private float determinar_velocidad(){
        return switch (estado){
            case Feliz -> 1.0f;
            case Neutro -> 0.6f;
            case Muerto -> 0.0f;
            default -> 0.2f;
        };
    }

    //Actualiza el estado de la mascota
    private void actualizar_estado(){
        estado = determinar_estado();
        estadoText.set(estado_string());
        velocidad.set(determinar_velocidad());
    }

    //Le agrega una cantidad dada a una propiedad, sin pasarse del máximo o mínimo
    private void sumar_stat(FloatProperty propiedad, Integer cantidad){
        int max = 0;
        if(propiedad == salud)
            max = SALUD_MAXIMA;
        if(propiedad == energia)
            max = ENERGIA_MAXIMA;
        if(propiedad == felicidad)
            max = FELICIDAD_MAXIMA;

        if (propiedad.getValue() + cantidad >= max)
            propiedad.set(max);
        else if (propiedad.getValue() + cantidad <= 0)
            propiedad.set(0);
        else
            propiedad.set(propiedad.getValue() + cantidad);
    }



    // Método encargado de modificar los indicadores por cada aumento de tiempo
    public void timeStep(){
        if (estado == Estado.Muerto)
            return;
        if (dormida.getValue()) {
            sumar_stat(energia, 20);
            sumar_stat(salud, 2);
            sumar_stat(felicidad, 2);
        } else if (edad.getValue() <= 5 && salud.getValue() <= 10){
            sumar_stat(felicidad, -20);
            sumar_stat(salud, -5);
            sumar_stat(energia, -5);
        } else if (edad.getValue() > 5 && edad.getValue() <= 10 && salud.getValue() <= 50){
            sumar_stat(felicidad, -20);
            sumar_stat(salud, -10);
            sumar_stat(energia, -5);
        } else if (edad.getValue() > 10 && salud.getValue() <= 50){
            sumar_stat(felicidad, -30);
            sumar_stat(salud, -20);
            sumar_stat(energia, -5);
        } else {
            sumar_stat(felicidad, -5);
            sumar_stat(salud, -5);
            sumar_stat(energia, -5);
        }
        edad.set(edad.getValue()+0.5);
        actualizar_estado();
    }


    //Le aplica los efectos del item dado a la mascota
    public void usar_item(Item item){
        if (estado == Estado.Muerto)
            return;
        if (item == null)
            return;
        sumar_stat(felicidad, item.get_felicidad());
        sumar_stat(salud, item.get_salud());
        sumar_stat(energia, item.get_energia());
        actualizar_estado();
    }

    //Setters para dormida
    public void dormir(){dormida.set(true);}
    public void despertar(){dormida.set(false);}

    //Getters de propiedades
    public BooleanProperty isDormida(){return dormida;}

    public StringProperty getNombre(){return nombre;}
    public StringProperty getEstado(){return estadoText;}
    public DoubleProperty getEdad(){return edad;}
    public FloatProperty getSalud(){return salud;}
    public FloatProperty getEnergia(){return energia;}
    public FloatProperty getFelicidad(){return felicidad;}
    public FloatProperty getVelocidad(){return velocidad;}

}
