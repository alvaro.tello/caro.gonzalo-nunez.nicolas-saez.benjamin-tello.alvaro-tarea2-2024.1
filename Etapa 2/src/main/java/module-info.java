module com.example.mascotavirtual {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;


    opens com.example.mascotavirtual to javafx.fxml;
    exports com.example.mascotavirtual;
}