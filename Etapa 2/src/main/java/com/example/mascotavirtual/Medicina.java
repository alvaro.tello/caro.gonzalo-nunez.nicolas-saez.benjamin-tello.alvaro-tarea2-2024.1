package com.example.mascotavirtual;

public class Medicina extends Item {
    
    public Medicina(Integer id, String nombre, Integer cantidad){
        super(id, nombre, cantidad);
        this.salud = 40;
    }

}