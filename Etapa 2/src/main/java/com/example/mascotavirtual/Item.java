package com.example.mascotavirtual;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.StringProperty;

public abstract class Item {
    private String nombre;
    private IntegerProperty cantidad;
    private Integer id;
    protected Integer salud = 0;
    protected Integer energia = 0;
    protected Integer felicidad = 0;

    protected Item(Integer id, String nombre, Integer cantidad) {
        this.id = id;
        this.nombre = nombre;
        if (cantidad >= 0)
            this.cantidad = new SimpleIntegerProperty(cantidad);
        else
            this.cantidad = null;
    }

    //Reduce en uno la cantidad del item
    public void reducir_cantidad(){
        if (cantidad == null || cantidad.getValue() <= 0)
            return;
        cantidad.set(cantidad.getValue()-1);
    }



    //Getters de atributos
    public Integer get_salud() {return salud;}
    public Integer get_energia() {return energia;}
    public Integer get_felicidad() {return felicidad;}
    public String get_nombre() {return nombre;}
    public IntegerProperty get_cantidad() {return cantidad;}
    public Integer get_id() {return id;}
}


