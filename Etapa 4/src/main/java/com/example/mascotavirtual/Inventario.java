package com.example.mascotavirtual;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Inventario {
    private ArrayList<Item> inventario;
    private ArrayList<Integer> cantidades_originales;

    public Inventario(){
        inventario = new ArrayList<Item>();
    }

    //Agrega el item al inventario
    public void agregar_items(Item item){ inventario.add(item); }

    //Resetea las cantidades de los items
    public void reset(){
        for (Item item: inventario)
            item.reset();
    }

    //Saca un item del inventario
    public Item sacar_item(Integer id){
        Item item_sacado = null;

        for (Item item : inventario){
            if (item.get_id() == id){
                item_sacado = item;
                item.reducir_cantidad();
                break;
            }
        }
        if (item_sacado == null)
            return null;
        if (item_sacado.get_cantidad() != null && item_sacado.get_cantidad().getValue() <= 0)
            return null;
        return item_sacado;
    }

    //Rellena las grids del inventario gráfico con los items
    public void rellenar_grids(GridPane alimentos, GridPane medicinas, GridPane juguetes, Mascota mascota) throws FileNotFoundException {
        int al = 0, med = 0, jug = 0;
        for (Item item : inventario){
            if (item instanceof Juguete){
                Infinito boton = new Infinito((Juguete) item, this, mascota);
                juguetes.add(boton, jug%2, jug/2);
                jug++;
            } else if (item instanceof Medicina) {
                Consumible panel = new Consumible(item, this, mascota);
                medicinas.add(panel, med%2, med/2);
                med++;
            } else {
                Consumible panel = new Consumible(item, this, mascota);
                alimentos.add(panel, al%2, al/2);
                al++;
            }
        }
    }
}
